package com.shop.demo.cart.repository;

import com.shop.demo.cart.model.Cart;
import com.shop.demo.cart.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CartRepository extends JpaRepository<Cart, Long> {

    @Query("select c from Cart c where c.user.id = :#{#user.id}")
    Cart findUserCart(@Param("user") User user);

}


