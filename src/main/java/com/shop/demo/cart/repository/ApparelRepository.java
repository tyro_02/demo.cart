package com.shop.demo.cart.repository;

import com.shop.demo.cart.model.Apparel;

public interface ApparelRepository extends ProductBaseRepository<Apparel> {
}
