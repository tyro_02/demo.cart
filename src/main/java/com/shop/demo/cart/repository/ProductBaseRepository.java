package com.shop.demo.cart.repository;

import com.shop.demo.cart.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@NoRepositoryBean
public interface ProductBaseRepository<T extends Product> extends CrudRepository<T, Long> {

    @Query("select p from Product p where p.prodName = :name")
    List<T> findProductByName(String name);

}



