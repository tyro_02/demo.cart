package com.shop.demo.cart.repository;

import com.shop.demo.cart.model.Product;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface ProductRepository extends ProductBaseRepository<Product> {
}
