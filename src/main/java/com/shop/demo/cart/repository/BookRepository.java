package com.shop.demo.cart.repository;

import com.shop.demo.cart.model.Book;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface BookRepository extends ProductBaseRepository<Book> {
}
