package com.shop.demo.cart.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity(name = "Apparel")
@Table(name = "apparel")
public class Apparel extends Product {
    private String type;
    private String brand;
    private String design;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getDesign() {
        return design;
    }

    public void setDesign(String design) {
        this.design = design;
    }

    public Apparel() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Apparel)) return false;
        if (!super.equals(o)) return false;
        Apparel apparel = (Apparel) o;
        return getType().equals(apparel.getType()) &&
                getBrand().equals(apparel.getBrand()) &&
                getDesign().equals(apparel.getDesign());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getType(), getBrand(), getDesign());
    }
}
