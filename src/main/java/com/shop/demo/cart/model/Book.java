package com.shop.demo.cart.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity(name = "Book")
@Table(name = "book")
public class Book extends Product {
    private String genre;
    private String author;
    private String publication;

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublication() {
        return publication;
    }

    public void setPublication(String publication) {
        this.publication = publication;
    }

    public Book(String genre, String author, String publication) {
        this.genre = genre;
        this.author = author;
        this.publication = publication;
    }

    public Book() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        if (!super.equals(o)) return false;
        Book book = (Book) o;
        return getGenre().equals(book.getGenre()) &&
                getAuthor().equals(book.getAuthor()) &&
                getPublication().equals(book.getPublication());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getGenre(), getAuthor(), getPublication());
    }
}
