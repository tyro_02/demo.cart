package com.shop.demo.cart.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Entity(name="Cart")
@Table(name="cart")
public class Cart {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    private BigDecimal total_price;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "cart_product", joinColumns = @JoinColumn(name = "cart_id"))
    @MapKeyJoinColumn(name = "product_id", referencedColumnName = "id")
    private Map<Product, Byte> products = new HashMap<>();


    public Cart(){

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BigDecimal getTotal_price() {
        return total_price;
    }

    public void setTotal_price(BigDecimal total_price) {
        this.total_price = total_price;
    }

    public Map<Product, Byte> getProducts() {
        return products;
    }

    public void setProducts(Map<Product, Byte> products) {
        this.products = products;
    }

    public void addProduct(Product product) {
        products.putIfAbsent(product, (byte)0);
        byte count = products.get(product);
        total_price = total_price.add(product.getPrice());
        products.put(product, ++count);
    }

    public void removeProduct(Product product) {
        products.computeIfPresent(product, (it, count) -> {
            total_price = total_price.subtract(it.getPrice());
            if (--count <= 0) {
                return null;
            }
            return count;
        });
    }

    public Cart emptyCart(User user, BigDecimal total_price) {
        this.user = user;
        this.total_price = total_price;
        this.products.clear();
        return this;
    }

    public Cart(User user, BigDecimal total_price) {
        this.user = user;
        this.total_price = total_price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cart)) return false;
        Cart cart = (Cart) o;
        return getId() == cart.getId() &&
                getUser().getId() == (cart.getUser().getId()) &&
                getTotal_price().equals(cart.getTotal_price()) &&
                getProducts().equals(cart.getProducts());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUser().getId(), getTotal_price(), getProducts());
    }
}
