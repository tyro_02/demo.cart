package com.shop.demo.cart.controller;

import com.shop.demo.cart.model.Cart;
import com.shop.demo.cart.model.Product;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

public class CartDTO {

    private BigDecimal total_price;

    private List<Tuple> products;

    public CartDTO() {
    }

    public CartDTO(Cart cart) {
        this.total_price = cart.getTotal_price();
        this.products = cart.getProducts().entrySet()
                .stream()
                .map(e -> new Tuple(e.getKey(), e.getValue()))
                .collect(Collectors.toList());
    }

    public BigDecimal getTotal_price() {
        return total_price.setScale(2, RoundingMode.CEILING);
    }

    public void setTotal_price(BigDecimal total_price) {
        this.total_price = total_price;
    }

    public List<Tuple> getProducts() {
        return products;
    }

    public void setProducts(List<Tuple> products) {
        this.products = products;
    }

    public static class Tuple {
        private Product product;
        private int count;

        public Tuple() {
        }

        public Tuple(Product product, int count) {
            this.product = product;
            this.count = count;
        }

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }
}
