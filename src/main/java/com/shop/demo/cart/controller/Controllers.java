package com.shop.demo.cart.controller;

import com.shop.demo.cart.model.*;
import com.shop.demo.cart.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
public class Controllers {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private ApparelRepository apparelRepository;

    Logger LOG = LoggerFactory.getLogger(Controllers.class);

    @GetMapping(path = "/user/{userId}/cart")
    public ResponseEntity  getCartDetails(@PathVariable(name = "userId") Long userId) {
        try{
            User user = userRepository.findById(userId).orElse(null);
            if(user==null){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User does not exists");
            }

            Cart userCart = cartRepository.findUserCart(user);

            if(userCart==null) {
                Cart cart = new Cart(user, BigDecimal.valueOf(0));
                userCart = cartRepository.save(cart);
            }

            return ResponseEntity.status(HttpStatus.OK).body(new CartDTO(userCart));
        } catch (Exception e) {
            LOG.info(e.getStackTrace().toString());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }

    }

    @PutMapping(path = "/user/{userId}/cart/products/{productId}")
    public ResponseEntity putItemIntoTheCart(@PathVariable(name = "userId") Long userId, @PathVariable(name = "productId") Long productId) {
        try {
            User user = userRepository.findById(userId).orElse(null);
            if(user==null){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User does not exists");
            }
            Cart userCart = cartRepository.findUserCart(user);

            if(userCart==null){
                Cart cart = new Cart(user, BigDecimal.valueOf(0));
                userCart = cartRepository.save(cart);
            }

            if (!productRepository.existsById(productId)) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product does not exists");
            }

            userCart.addProduct(productRepository.findById(productId).orElseThrow(IllegalStateException::new));
            cartRepository.save(userCart);

            return ResponseEntity.status(HttpStatus.OK).body(new CartDTO(userCart));
        } catch (Exception e){
            LOG.info(e.getStackTrace().toString());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @DeleteMapping(path = "/user/{userId}/cart/products/{productId}")
    public ResponseEntity removeItemFromTheCart(@PathVariable(name = "userId") Long userId, @PathVariable(name = "productId") Long productId) {
        try {
            User user = userRepository.findById(userId).orElse(null);
            if(user==null){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User does not exists");
            }
            Cart userCart = cartRepository.findUserCart(user);

            if(userCart==null){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User cart is empty");
            }

            if (!productRepository.existsById(productId)) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product does not exists");
            }

            userCart.removeProduct(productRepository.findById(productId).orElseThrow(IllegalStateException::new));
            cartRepository.save(userCart);

            return ResponseEntity.status(HttpStatus.OK).body(new CartDTO(userCart));
        } catch (Exception e) {
            LOG.info(e.getStackTrace().toString());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }

    }

    @GetMapping(path = "/products")
    public Iterable<Product> allProducts() {
        return productRepository.findAll();
    }

    @GetMapping(path = "/products/{id}")
    public ResponseEntity getProductById(@PathVariable(name = "id") Long id) {
        try {
            Product product = productRepository.findById(id).orElse(null);

            if(product == null)
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            else
                return ResponseEntity.status(HttpStatus.OK).body(product);
        } catch (Exception e){
            LOG.info(e.getStackTrace().toString());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping(path = "/products/by/name/{name}")
    public ResponseEntity getProductsByName(@PathVariable(name = "name") String prod_name) {
        try {
            List<Product> products = productRepository.findProductByName(prod_name);
            return ResponseEntity.status(HttpStatus.OK).body(products);
        } catch (Exception e){
            LOG.info(e.getStackTrace().toString());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping(path = "/products/books")
    public ResponseEntity getOnlyBooks() {
        try {
            Iterable<Book> books = bookRepository.findAll();
            return ResponseEntity.status(HttpStatus.OK).body(books);
        } catch (Exception e){
            LOG.info(e.getStackTrace().toString());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping(path = "/products/apparels")
    public ResponseEntity getOnlyApparels() {
        try {
            Iterable<Apparel> apparels = apparelRepository.findAll();
            return ResponseEntity.status(HttpStatus.OK).body(apparels);
        } catch (Exception e){
            LOG.info(e.getStackTrace().toString());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @DeleteMapping(path = "/user/{userId}/cart")
    public ResponseEntity  emptyCartDetails(@PathVariable(name = "userId") Long userId) {
        try{
            User user = userRepository.findById(userId).orElse(null);
            if(user==null){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User does not exists");
            }

            Cart userCart = cartRepository.findUserCart(user);
            Cart cart = userCart.emptyCart(user, BigDecimal.valueOf(0));

            userCart = cartRepository.save(cart);

            return ResponseEntity.status(HttpStatus.OK).body(new CartDTO(userCart));
        } catch (Exception e) {
            LOG.info(e.getStackTrace().toString());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }

    }
}
