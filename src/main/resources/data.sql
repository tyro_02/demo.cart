INSERT INTO PRODUCT(ID, PROD_NAME, PRICE) VALUES (1, 'Harry Potter', 200.55);
INSERT INTO PRODUCT(ID, PROD_NAME, PRICE) VALUES (2, 'Chhawa', 450.45);
INSERT INTO PRODUCT(ID, PROD_NAME, PRICE) VALUES (3, 'Chatrapati Shivaji Maharaj', 1000.00);
INSERT INTO PRODUCT(ID, PROD_NAME, PRICE) VALUES (4, 'Asa Mi Asami', 99.99);
INSERT INTO PRODUCT(ID, PROD_NAME, PRICE) VALUES (5, 'Kurta', 35.75);
INSERT INTO PRODUCT(ID, PROD_NAME, PRICE) VALUES (6, 'Shirt', 10.20);
INSERT INTO PRODUCT(ID, PROD_NAME, PRICE) VALUES (7, 'Dress', 50.00);
INSERT INTO PRODUCT(ID, PROD_NAME, PRICE) VALUES (8, 'Suit', 200.00);

INSERT INTO BOOK(ID, GENRE, AUTHOR, PUBLICATION) VALUES (1, 'Contemporary Fantasy', 'J. K. Rollings', '‎Bloomsbury Publisher');
INSERT INTO BOOK(ID, GENRE, AUTHOR, PUBLICATION) VALUES (2, 'Action', 'Shivaji Savant', 'Mehta Publishing House');
INSERT INTO BOOK(ID, GENRE, AUTHOR, PUBLICATION) VALUES (3, 'Action', 'Krishanrao Arjun Kelusakar', 'Saraswati Publishing Co.Pvt.Ltd');
INSERT INTO BOOK(ID, GENRE, AUTHOR, PUBLICATION) VALUES (4, 'Comedy', 'Pu La Deshpande', 'SANSKRUTI BOOK HOUSE');

INSERT INTO APPAREL(ID, TYPE, BRAND, DESIGN) VALUES (5, 'Woman Wear', 'Pantaloons', 'Plain');
INSERT INTO APPAREL(ID, TYPE, BRAND, DESIGN) VALUES (6, 'Unisex', 'Nike', 'Formals');
INSERT INTO APPAREL(ID, TYPE, BRAND, DESIGN) VALUES (7, 'Woman Wear', 'Khadi', 'Khadi');
INSERT INTO APPAREL(ID, TYPE, BRAND, DESIGN) VALUES (8, 'Unisex', 'River Island', '3 Piece');

INSERT INTO USER(ID, NAME) VALUES (1, 'Babuarao Apte');
INSERT INTO USER(ID, NAME) VALUES (2, 'Natwarlal B');
INSERT INTO USER(ID, NAME) VALUES (3, 'Chachi 420');
INSERT INTO USER(ID, NAME) VALUES (4, 'Anthony Gonsalvis');
