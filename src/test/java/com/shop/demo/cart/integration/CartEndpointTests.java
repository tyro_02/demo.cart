package com.shop.demo.cart.integration;

import com.shop.demo.cart.Application;
import com.shop.demo.cart.config.Config;
import com.shop.demo.cart.controller.CartDTO;
import com.shop.demo.cart.utils.Utils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest(classes = { Application.class, Config.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CartEndpointTests {
    @LocalServerPort
    private int port;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    TestRestTemplate testRestTemplate;

    @Autowired
    String lolcahost;

    HttpHeaders headers = new HttpHeaders();

    @Test
    public void shouldGetEmptyCartForAnyUser(){
        String cartBaseUrl = lolcahost + ":" + port + "/user/1/cart";

        ResponseEntity<CartDTO> response = restTemplate.exchange(
                cartBaseUrl,
                HttpMethod.GET,
                null,
                CartDTO.class);
        assertThat("failure, status code did not match", response.getStatusCode(), equalTo(HttpStatus.OK));

        CartDTO cart = response.getBody();
        assertThat("failure, product list size did not match", cart.getProducts().size(), equalTo(0));
        assertThat("failure, total price did not match", cart.getTotal_price(), equalTo(Utils.getNumberWith2DecimalPlaces(0)));
    }

    @Test
    public void shouldAddAndRemoveProductToCartForAnyUser(){
        String url = lolcahost + ":" + port + "/user/1/cart/products/";
        Long prod1Id = 1l;

        headers.set("Content-Type", "application/json");
        HttpEntity<?> request = new HttpEntity<>(headers);

        ResponseEntity<CartDTO> putResponse = restTemplate.exchange(
                url + prod1Id,
                HttpMethod.PUT,
                request,
                CartDTO.class);
        assertThat("failure, status code did not match", putResponse.getStatusCode(), equalTo(HttpStatus.OK));

        CartDTO cart = putResponse.getBody();
        assertThat("failure, product list size did not match", cart.getProducts().size(), equalTo(1));
        assertThat("failure, product list size did not match", cart.getProducts().get(0).getCount(), equalTo(1));
        assertThat("failure, total price did not match", cart.getTotal_price(), equalTo(Utils.getNumberWith2DecimalPlaces(200.55)));

        ResponseEntity<CartDTO> deleteResponse = restTemplate.exchange(
                url + prod1Id,
                HttpMethod.DELETE,
                request,
                CartDTO.class);
        assertThat("failure, status code did not match", deleteResponse.getStatusCode(), equalTo(HttpStatus.OK));

        CartDTO emptyCart = deleteResponse.getBody();
        assertThat("failure, product list size did not match", emptyCart.getProducts().size(), equalTo(0));
        assertThat("failure, total price did not match", emptyCart.getTotal_price(), equalTo(Utils.getNumberWith2DecimalPlaces(0)));
    }

    @Test
    public void shouldAddAProductMultipleTimesToCartForAnyUser(){
        String url = lolcahost + ":" + port + "/user/1/cart/products/";
        Long prod1Id = 1l;

        headers.set("Content-Type", "application/json");
        HttpEntity<?> request = new HttpEntity<>(headers);

        restTemplate.put(url + prod1Id, request);
        restTemplate.put(url + prod1Id, request);

        ResponseEntity<CartDTO> putResponse = restTemplate.exchange(
                url + prod1Id,
                HttpMethod.PUT,
                request,
                CartDTO.class);
        assertThat("failure, status code did not match", putResponse.getStatusCode(), equalTo(HttpStatus.OK));

        CartDTO cart = putResponse.getBody();
        assertThat("failure, product list size did not match", cart.getProducts().size(), equalTo(1));
        assertThat("failure, product list size did not match", cart.getProducts().get(0).getCount(), equalTo(3));
        assertThat("failure, total price did not match", cart.getTotal_price(), equalTo(Utils.getNumberWith2DecimalPlaces(601.65)));
    }

    @Test
    public void shouldAddMultipleProductsMultipleTimesToCartForAnyUser(){
        String url = lolcahost + ":" + port + "/user/2/cart/products/";
        Long prod1Id = 1l;
        Long prod2Id = 6l;

        headers.set("Content-Type", "application/json");
        HttpEntity<?> request = new HttpEntity<>(headers);

        restTemplate.put(url + prod1Id, request);
        restTemplate.put(url + prod1Id, request);
        restTemplate.put(url + prod2Id, request);

        ResponseEntity<CartDTO> putResponse = restTemplate.exchange(
                url + prod2Id,
                HttpMethod.PUT,
                request,
                CartDTO.class);
        assertThat("failure, status code did not match", putResponse.getStatusCode(), equalTo(HttpStatus.OK));

        CartDTO cart = putResponse.getBody();
        assertThat("failure, product list size did not match", cart.getProducts().size(), equalTo(2));
        assertThat("failure, total price did not match", cart.getTotal_price(), equalTo(Utils.getNumberWith2DecimalPlaces(421.50)));
    }

    @Test
    public void shouldGiveNotFoundWhenNonExistentProductAddedToCartForAnyUser(){
        String url = lolcahost + ":" + port + "/user/2/cart/products/";
        Long prod1Id = 100l;

        headers.set("Content-Type", "application/json");
        HttpEntity<?> request = new HttpEntity<>(headers);

        ResponseEntity<?> putResponse = testRestTemplate.exchange(
                url + prod1Id,
                HttpMethod.PUT,
                request,
                String.class);
        assertThat("failure, status code did not match", putResponse.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
    }

    @Test
    public void shouldGiveBadRequestWhenBadProductAddedToCartForAnyUser(){
        String url = lolcahost + ":" + port + "/user/2/cart/products/";
        String prod1Id = "100ABC";

        headers.set("Content-Type", "application/json");
        HttpEntity<?> request = new HttpEntity<>(headers);

        ResponseEntity<?> putResponse = testRestTemplate.exchange(
                url + prod1Id,
                HttpMethod.PUT,
                request,
                String.class);
        assertThat("failure, status code did not match", putResponse.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void shouldItemTotalNoLessThanZeroForAnyUser(){
        String cartBaseUrl = lolcahost + ":" + port + "/user/3/cart";
        String url = lolcahost + ":" + port + "/user/3/cart/products/";
        Long prod1Id = 1l;

        headers.set("Content-Type", "application/json");
        HttpEntity<?> request = new HttpEntity<>(headers);

        restTemplate.put(url + prod1Id, request);
        restTemplate.put(url + prod1Id, request);

        restTemplate.delete(url + prod1Id, request);
        restTemplate.delete(url + prod1Id, request);
        restTemplate.delete(url + prod1Id, request);

        ResponseEntity<CartDTO> response = restTemplate.exchange(
                cartBaseUrl,
                HttpMethod.GET,
                null,
                CartDTO.class);
        assertThat("failure, status code did not match", response.getStatusCode(), equalTo(HttpStatus.OK));

        CartDTO cart = response.getBody();
        assertThat("failure, product list size did not match", cart.getProducts().size(), equalTo(0));
        assertThat("failure, total price did not match", cart.getTotal_price(), equalTo(Utils.getNumberWith2DecimalPlaces(0)));
    }

    @Test
    public void shouldEmptyCartForAnyUser(){
        String cartBaseUrl = lolcahost + ":" + port + "/user/3/cart";
        String url = lolcahost + ":" + port + "/user/3/cart/products/";
        Long prod1Id = 1l;
        Long prod2Id = 7l;

        headers.set("Content-Type", "application/json");
        HttpEntity<?> request = new HttpEntity<>(headers);

        restTemplate.put(url + prod1Id, request);
        restTemplate.put(url + prod1Id, request);
        restTemplate.put(url + prod1Id, request);
        restTemplate.put(url + prod2Id, request);
        restTemplate.put(url + prod2Id, request);

        ResponseEntity<CartDTO> response = restTemplate.exchange(
                cartBaseUrl,
                HttpMethod.DELETE,
                request,
                CartDTO.class);
        assertThat("failure, status code did not match", response.getStatusCode(), equalTo(HttpStatus.OK));

        CartDTO cart = response.getBody();
        assertThat("failure, product list size did not match", cart.getProducts().size(), equalTo(0));
        assertThat("failure, total price did not match", cart.getTotal_price(), equalTo(Utils.getNumberWith2DecimalPlaces(0)));
    }
}
