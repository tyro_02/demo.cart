package com.shop.demo.cart.integration;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import com.shop.demo.cart.Application;
import com.shop.demo.cart.config.Config;
import com.shop.demo.cart.model.Book;
import com.shop.demo.cart.model.Product;
import com.shop.demo.cart.utils.Utils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@SpringBootTest(classes = { Application.class, Config.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductEndpointsTests {

    @LocalServerPort
    private int port;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    TestRestTemplate testRestTemplate;

    @Autowired
    String lolcahost;

    @Test
    public void shouldGetAllProducts() {
        String productsBaseUrl = lolcahost + ":" + port + "/products";

        ResponseEntity<List<Product>> response = restTemplate.exchange(
                productsBaseUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Product>>(){});
        assertThat("failure, status code did not match", response.getStatusCode(), equalTo(HttpStatus.OK));

        List<Product> products = response.getBody();
        assertThat("failure, list size did not match", products.size(), equalTo(8));
    }

    @Test
    public void shouldGetProductsByCategoryBooks() {
        String url = lolcahost + ":" + port + "/products/books";

        ResponseEntity<List<Product>> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Product>>(){});
        assertThat("failure, status code did not match", response.getStatusCode(), equalTo(HttpStatus.OK));

        List<Product> products = response.getBody();
        assertThat("failure, list size did not match", products.size(), equalTo(4));
    }

    @Test
    public void shouldGetProductsByProductName() {
        String url = lolcahost + ":" + port + "/products/by/name/Harry Potter";

        ResponseEntity<List<Product>> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Product>>(){});
        assertThat("failure, status code did not match", response.getStatusCode(), equalTo(HttpStatus.OK));

        List<Product> products = response.getBody();
        assertThat("failure, list size did not match", products.size(), equalTo(1));
    }

    @Test
    public void shouldGetProductsByCategoryApparels() {
        String url = lolcahost + ":" + port + "/products/apparels";

        ResponseEntity<List<Product>> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Product>>(){});
        assertThat("failure, status code did not match", response.getStatusCode(), equalTo(HttpStatus.OK));

        List<Product> products = response.getBody();
        assertThat("failure, list size did not match", products.size(), equalTo(4));
    }

    @Test
    public void shouldGetProductById() {
        String url = lolcahost + ":" + port + "/products/1";

        ResponseEntity<Book> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                Book.class);
        assertThat("failure, status code did not match", response.getStatusCode(), equalTo(HttpStatus.OK));

        Book book = response.getBody();

        assertThat("failure, ID did not match", book.getId(), equalTo(1L));
        assertThat("failure, Price did not match", book.getPrice(), equalTo(Utils.getNumberWith2DecimalPlaces(200.55)));
        assertThat("failure, Product name did not match", book.getProdName(), equalTo("Harry Potter"));
        assertThat("failure, Author did not match", book.getAuthor(), equalTo("J. K. Rollings"));
        assertThat("failure, Genre did not match", book.getGenre(), equalTo("Contemporary Fantasy"));
        assertThat("failure, Publication did not match", book.getPublication().replaceAll("\\P{Print}", ""), equalTo("Bloomsbury Publisher"));
        // In publication there is a non printable unicode character left-to-right ('\u200E' 8206)
        // Currently in order to assert correctly, we need to remove it
        // Need to look why/how that character gets into response json in first place that to only for ID=1
    }

    @Test
    public void shouldGetNotFoundForNonExistentProductId() {
        String url = lolcahost + ":" + port + "/products/100";

        ResponseEntity<String> response = testRestTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                String.class);
        assertThat("failure, status code did not match", response.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
    }

    @Test
    public void shouldGetBadRequestForInvalidProductId() {
        String productBaseUrl = lolcahost + ":" + port + "/products/ABC100";

        ResponseEntity<String> response = testRestTemplate.exchange(
                productBaseUrl,
                HttpMethod.GET,
                null,
                String.class);
        assertThat("failure, status code did not match", response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }
}
