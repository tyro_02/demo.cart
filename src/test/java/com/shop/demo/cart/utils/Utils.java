package com.shop.demo.cart.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Utils {

    public static BigDecimal getNumberWith2DecimalPlaces(double val){
        return (BigDecimal.valueOf(val).setScale(2, RoundingMode.CEILING));
    }
}
