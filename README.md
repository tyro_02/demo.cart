# Shopping cart REST APIs
This is a demo shopping cart APIs uses H2 database. Please refer to test data in /main/resources/data.sql

## Endpoints

### Product
* GET on /products - To get all the products
* GET on /products/{id} - To get product by ID 
* GET on /products/by/name/{name} - To get products by name
* GET on /products/books - To get products by Book category
* GET on /products/apparels - To get products by Apparel category

All the tests are in /test/com/shop/demo/cart/integration/ProductEndpointsTests.java

### Cart
* GET on /user/{userId}/cart - To get cart details for given user
* DELETE on /user/{userId}/cart - To empty/clear the cart for given user
* PUT on /user/{userId}/cart/products/{productId} - To add a product to the cart for given user
* DELETE on /user/{userId}/cart/products/{productId} - To remove a product from the cart for given user 

All the tests are in /test/com/shop/demo/cart/integration/CartEndpointsTests.java